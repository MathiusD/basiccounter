﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class Counter
    {
        private int value;
        public Counter()
        {
            this.value = 0;
        }
        private void SetValue(int value)
        {
            this.value = value;
        }
        public int GetValue()
        {
            return this.value;
        }
        public void RAZ()
        {
            SetValue(0);
        }
        public void Increase ()
        {
            SetValue(GetValue() + 1);
        }
        public void Decrease()
        {
            if (GetValue() > 0)
            {
                SetValue(GetValue() - 1);
            }
        }
    }
}

