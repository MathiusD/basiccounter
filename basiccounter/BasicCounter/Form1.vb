﻿Imports BasicCounterLib
Public Class Form1
    Public Compt As Counter
    Private Sub RAZ_Click(sender As Object, e As EventArgs) Handles RAZ.Click
        Compt.RAZ()
        Affich.Text = Compt.GetValue()
    End Sub
    Private Sub Minus_Click(sender As Object, e As EventArgs) Handles Minus.Click
        Compt.Decrease()
        Affich.Text = Compt.GetValue()
    End Sub
    Private Sub Plus_Click(sender As Object, e As EventArgs) Handles Plus.Click
        Compt.Increase()
        Affich.Text = Compt.GetValue()
    End Sub
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Compt = New Counter()
        Affich.Text = Compt.GetValue()
    End Sub
End Class