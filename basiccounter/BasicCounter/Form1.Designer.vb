﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Total = New System.Windows.Forms.Label()
        Me.Affich = New System.Windows.Forms.Label()
        Me.Plus = New System.Windows.Forms.Button()
        Me.Minus = New System.Windows.Forms.Button()
        Me.RAZ = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Total
        '
        Me.Total.AutoSize = True
        Me.Total.Location = New System.Drawing.Point(96, 9)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(31, 13)
        Me.Total.TabIndex = 0
        Me.Total.Text = "Total"
        '
        'Affich
        '
        Me.Affich.AutoSize = True
        Me.Affich.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Affich.Location = New System.Drawing.Point(96, 32)
        Me.Affich.Name = "Affich"
        Me.Affich.Size = New System.Drawing.Size(0, 17)
        Me.Affich.TabIndex = 1
        '
        'Plus
        '
        Me.Plus.Location = New System.Drawing.Point(141, 27)
        Me.Plus.Name = "Plus"
        Me.Plus.Size = New System.Drawing.Size(75, 23)
        Me.Plus.TabIndex = 2
        Me.Plus.Text = "+"
        Me.Plus.UseVisualStyleBackColor = True
        '
        'Minus
        '
        Me.Minus.Location = New System.Drawing.Point(15, 27)
        Me.Minus.Name = "Minus"
        Me.Minus.Size = New System.Drawing.Size(75, 23)
        Me.Minus.TabIndex = 3
        Me.Minus.Text = "-"
        Me.Minus.UseVisualStyleBackColor = True
        '
        'RAZ
        '
        Me.RAZ.Location = New System.Drawing.Point(77, 56)
        Me.RAZ.Name = "RAZ"
        Me.RAZ.Size = New System.Drawing.Size(75, 23)
        Me.RAZ.TabIndex = 4
        Me.RAZ.Text = "RAZ"
        Me.RAZ.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(255, 100)
        Me.Controls.Add(Me.RAZ)
        Me.Controls.Add(Me.Minus)
        Me.Controls.Add(Me.Plus)
        Me.Controls.Add(Me.Affich)
        Me.Controls.Add(Me.Total)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Total As Label
    Friend WithEvents Affich As Label
    Friend WithEvents Plus As Button
    Friend WithEvents Minus As Button
    Friend WithEvents RAZ As Button
End Class
