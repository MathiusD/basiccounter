﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace BasicCounterTU
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetValue_Test()
        {
            Counter Counter = new Counter();
            Assert.AreEqual(0, Counter.GetValue());
        }
        [TestMethod]
        public void Increase_Test()
        {
            Counter Counter = new Counter();
            Counter.Increase();
            Assert.AreEqual(1, Counter.GetValue());
        }
        [TestMethod]
        public void Decrease_Test()
        {
            Counter Counter = new Counter();
            Counter.Increase();
            Counter.Increase();
            Counter.Decrease();
            Assert.AreEqual(1, Counter.GetValue());
            Counter Counter2 = new Counter();
            Counter.Decrease();
            Assert.AreEqual(0, Counter.GetValue());
        }
        [TestMethod]
        public void RAZ_Test()
        {
            Counter Counter = new Counter();
            Counter.Increase();
            Counter.Increase();
            Counter.Decrease();
            Counter.RAZ();
            Assert.AreEqual(0, Counter.GetValue());
        }
    }
}
